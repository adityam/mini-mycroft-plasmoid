/*
    Copyright 2019 Marco Martin <mart@kde.org>
    Copyright 2019 Aditya Mehra <aix.m@outlook.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.
    
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public
    License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.9
import QtQml.Models 2.2
import QtQuick.Controls 2.2 as Controls
import QtQuick.Layouts 1.3
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.kirigami 2.5 as Kirigami
import Mycroft 1.0 as Mycroft

Item {
    id: root

    implicitWidth: Kirigami.Units.gridUnit * 20
    implicitHeight: Kirigami.Units.gridUnit * 32

    Layout.minimumWidth: Kirigami.Units.gridUnit * 10
    Layout.minimumHeight: Kirigami.Units.gridUnit * 15
    property bool speakBarEnabled: plasmoid.configuration.speakBar
    property bool onDesktop: (plasmoid.location !== PlasmaCore.Types.TopEdge
        || plasmoid.location !== PlasmaCore.Types.RightEdge
        || plasmoid.location !== PlasmaCore.Types.BottomEdge
        || plasmoid.location !== PlasmaCore.Types.LeftEdge)
    
    Component.onCompleted: {
        if(!isRemoved) {
            Mycroft.MycroftController.start();
        }
    }
    
    onVisibleChanged: {
        if(onDesktop && !visible && Mycroft.MycroftController.status == Mycroft.MycroftController.Open){
            isRemoved = true
            Mycroft.MycroftController.disconnectSocket();
        }
    }
            
    Timer {
        interval: 10000
        running: Mycroft.MycroftController.status != Mycroft.MycroftController.Open
        onTriggered: {
            if(!isRemoved) {
                print("Trying to connect to Mycroft");
                Mycroft.MycroftController.start();
            }
        }
    }
    
    Connections {
        target: Mycroft.MycroftController
        onListeningChanged: {
            if (Mycroft.MycroftController.listening && !plasmoid.expanded) {
                plasmoid.expanded = true
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        
        Mycroft.SkillView {
            id: skillView
            Layout.fillWidth: true
            Layout.preferredHeight: speakBarEnabled ? parent.height - (bottomBar.height + Kirigami.Units.largeSpacing) : parent.height
            backgroundVisible: false

            Kirigami.Theme.colorSet: Kirigami.Theme.View

            clip: true
            onCurrentItemChanged: {
                currentItem.background.visible = false
                inputField.forceActiveFocus();
            }
        }
        
        BottomBarViewComponent {
            id: bottomBar
            Layout.fillWidth: true
            visible: speakBarEnabled ? 1 : 0
            enabled: speakBarEnabled ? 1 : 0
            Layout.preferredHeight: speakBarEnabled ? Kirigami.Units.gridUnit * 2.5 : 0
            Layout.bottomMargin: Kirigami.Units.largeSpacing * 1.5
            Layout.rightMargin: Kirigami.Units.largeSpacing * 0.5
        }
    }
    
    Mycroft.StatusIndicator {
        anchors.horizontalCenter: parent.horizontalCenter
        y: skillView.currentItem == skillView.initialItem ? parent.height/2 - height/2 : parent.height - height - Kirigami.Units.largeSpacing
    }
}
