/* Copyright 2016 Aditya Mehra <aix.m@outlook.com>                            

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.
    
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public
    License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.9
import QtQml.Models 2.2
import QtQuick.Controls 2.2 as Controls
import QtQuick.Layouts 1.3
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.kirigami 2.5 as Kirigami
import Mycroft 1.0 as Mycroft

Item {
    id: appletBottomBarComponent
    
    RowLayout {
        anchors.fill: parent
        
        PlasmaComponents.Button {
            id: homeButton
            iconSource: "go-home"
            Layout.preferredWidth: Kirigami.Units.gridUnit * 2.5
            Layout.fillHeight: true
            onClicked: {
                Mycroft.MycroftController.sendRequest("mycroft.gui.screen.close", {});
            }
        }
        
        PlasmaComponents.TextField {
            id: inputQuery
            Layout.fillHeight: true
            Layout.fillWidth: true
            placeholderText: i18n("Enter Query or Say 'Hey Mycroft'")
            clearButtonShown: true

            onAccepted: {
                Mycroft.MycroftController.sendText(inputQuery.text);
            }

            Connections {
                target: Mycroft.MycroftController
                onIntentRecevied: { 
                    if(type == "recognizer_loop:utterance") {
                        inputQuery.text = data.utterances[0]
                    }
                }
            }
        }
    
        PlasmaComponents.Button {
            id: speakButton
            text: "Speak"
            Layout.fillHeight: true
            Layout.preferredWidth: Kirigami.Units.gridUnit * 4
            onClicked: {
                Mycroft.MycroftController.sendRequest("mycroft.mic.listen", {})
            }
        }
    }
}
